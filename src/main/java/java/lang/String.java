package java.lang;

/**
 * @author bcliao
 * @date 2022/8/22 23:13
 */
public class String {

    // 运行报错，在类 java.lang.String 中找不到 main 方法
    // 因为压根不会去加载这个自己创建的类
    // 由于双亲委派机制 在 BootStrapClassLoader 已经加载了 java.lang.String
    public static void main(String[] args) {

    }
}
