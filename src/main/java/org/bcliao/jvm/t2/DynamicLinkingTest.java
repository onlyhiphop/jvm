package org.bcliao.jvm.t2;

/**
 * @author bcliao
 * @date 2022/9/12 9:58
 */
public class DynamicLinkingTest {

    int num = 10;

    public void methodA(){
        System.out.println("methodA()....");
    }

    public void methodB(){
        System.out.println("methodB()...");

        methodA();

        num++;
    }

}
