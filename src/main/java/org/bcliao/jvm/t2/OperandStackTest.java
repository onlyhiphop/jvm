package org.bcliao.jvm.t2;

/**
 * @author bcliao
 * @date 2022/9/11 15:18
 */
public class OperandStackTest {

    public int testAddOperation(){
        // byte short char boolean 都是以 int 类型来保存的
        byte i = -128;
        int j = 8;
        int k = i + j;

        return k;
    }

    public void add(){
        /**
         * 发现 i1++ 是先 iload_1 再  iinc 1 by 1
         * ++i3 是先   iinc 3 by 1 再  iload_3
         */
        int i1 = 10;
        int i2 = i1++;

        int i3 = 10;
        int i4 = ++i3;

        System.out.println(i1);  //11
        System.out.println(i2);  //10
        System.out.println(i3);  //11
        System.out.println(i4);  //11
    }

    public static void main(String[] args) {
        new OperandStackTest().add();
    }
}
