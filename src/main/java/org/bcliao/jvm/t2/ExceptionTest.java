package org.bcliao.jvm.t2;

/**
 * @author bcliao
 * @date 2022/9/12 17:45
 */
public class ExceptionTest {

    public static void main(String[] args) {

        try {
            new ExceptionTest().exception();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public void exception() throws InterruptedException {

        Thread.sleep(1000);

    }

}
