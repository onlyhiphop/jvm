package org.bcliao.jvm.t2;

/**
 * @author bcliao
 * @date 2022/8/25 10:13
 */
public class LocalVariableTest {

    public static void main(String[] args) {
        int num = 0;
        boolean flag = true;
        LocalVariableTest a = new LocalVariableTest();
    }

    public String test(){
        String name = "Test";
        return name;
    }

    public int test2(){
        return 1;
    }

    public void test3(){
        int a = 0;
        {
            // 出了这个 {} 代码块 b 的槽位就回收
            int b  = 0;
            b  = a + 1;
        }
        // 变量 c  使用之前已经销毁的变量 b 占据的槽位
        int c = a + 1;
    }
}
