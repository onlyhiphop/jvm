package org.bcliao.jvm.t1;

/**
 *
 *
 * @author bcliao
 * @date 2022/8/22 10:48
 */
public class ClassInitTest {

    private static int num = 1;

    static {
        num = 2;
        System.out.println(num);

        // 可以赋值，却不能使用
        number = 20;
//        System.out.println(number);  //报错：菲方的前向引用
    }

    private static int number = 10; // linking 之后 prepare: number = 0 --> initial :20 --> 10

    public static void main(String[] args) {
        System.out.println(ClassInitTest.num);  // 2
        System.out.println(ClassInitTest.number); //10
    }

}
