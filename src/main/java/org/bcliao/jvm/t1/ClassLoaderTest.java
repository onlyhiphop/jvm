//package org.bcliao.jvm.t1;
//
//import sun.misc.Launcher;
//
//import java.net.URL;
//
///**
// * @author bcliao
// * @date 2022/8/22 15:04
// */
//public class ClassLoaderTest {
//
//    public static void main(String[] args) {
//
//        // 获取系统类加载器
//        ClassLoader systemClassLoader = ClassLoader.getSystemClassLoader();
//        System.out.println(systemClassLoader);  // sun.misc.Launcher$AppClassLoader@18b4aac2
//
//        // 获取其上层：扩展类加载器
//        ClassLoader extClassLoader = systemClassLoader.getParent();
//        System.out.println(extClassLoader);  // sun.misc.Launcher$ExtClassLoader@75b84c92
//
//        // 尝试再获取上层
//        ClassLoader bootStrapClassLoader = extClassLoader.getParent();
//        System.out.println(bootStrapClassLoader);  // null 无法获取
//
//        // 对于用户自定义的类来说  默认使用系统类加载器来加载
//        ClassLoader classLoader = ClassLoaderTest.class.getClassLoader();
//        System.out.println(classLoader); // sun.misc.Launcher$AppClassLoader@18b4aac2
//
//        // String 类使用引导类加载器进行加载的。 --> Java 的核心类库都是使用引导类加载器进行加载的
//        ClassLoader stringLoader = String.class.getClassLoader();
//        System.out.println(stringLoader);  // null  说明使用了 BootStrapClassLoader 加载的
//    }
//
//    public static void getBootstrapClass(){
//        System.out.println("***** 启动类加载器 *****");
//        // 获取 BootstrapClassLoader 能够加载的 api 的路径
//        URL[] urLs = Launcher.getBootstrapClassPath().getURLs();
//        for (URL urL : urLs) {
//            System.out.println(urL.toExternalForm());
//        }
//    }
//
//    public static void getExtClass(){
//        System.out.println("***** 扩展类加载器 *****");
//        String extDirs = System.getProperty("java.ext.dirs");
//        for (String path : extDirs.split(";")) {
//            System.out.println(path);
//        }
//    }
//}
