package org.bcliao.jvm.t1;

/**
 * @author bcliao
 * @date 2022/8/22 14:43
 */
public class FatherInitTest {

    static class Father{
        public static int A = 1;
        static {
            A = 2;
        }
    }

    static class Son extends Father {
        public static int B = A;
    }

    public static void main(String[] args) {
        // 先加载 Father ， 其次加载 Son
        System.out.println(Son.B);  // 2
    }

}
