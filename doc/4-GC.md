#### STW

Stop-The-World 是在执行垃圾收集算法时，Java应用程序的其他所有线程都会被 挂起 ；是Java中的一种全局暂停现象，native 代码可以执行，但是不能与 JVM 交互



#### Minor GC、Major GC、Full GC

JVM 在进行 GC 时，并非每次都对 新生代、老年代、方法区 一起回收，大部分回收的都是新生代。针对 hotspot vm 的实现，它里面的GC按照回收区域又分为两大种类型：一种是部分收集（Partial GC），一种是整堆收集（Full GC）

- 部分收集：不是完整收集整个 Java 堆的垃圾收集。其中又分为：	
  - 新生代收集（Minor GC / Young GC）：只是新生代的垃圾收集
  - 老年代收集（Major GC / Old GC）：只是老年代的垃圾收集
    - 目前，只有CMS GC 会有**单独**收集老年代的行为，其他GC在收集老年代的同时也会对新生代进行收集
    - 注意，很多时候 Major GC 会和 FUll GC 混淆使用，需要具体分辨是老年代回收还是整堆回收
  - 混合收集（Mixed GC）：收集整个新生代以及部分老年代的垃圾收集。
    - 目前，只有 G1 GC 会有这种行为
- 整堆收集（Full GC）：收集整个 java 堆和方法区的垃圾收集



##### 年轻代GC（Minor GC）触发机制：

- 当年轻代空间不足时，就会出发 Minor GC，这里的年轻代满指的是 Eden 区满，Survivor 满不会引发GC。（每次 Minor GC 会清理年轻代的内存）
- 因为 Java 对象大多都是 朝生夕灭 ，所以Minor GC 非常频繁，一般回收速度也比较快。
- Minor GC 会应发 STW ，暂停其他用户的线程，等垃圾回收结束，用户线程才恢复运行。



##### 老年代GC（Major GC / Full GC）出发机制：

- 指发生在老年代的 GC ，对象从老年代消失时，我们说 “Major GC” 或 “Full GC” 发生了
- 出现了 Major GC，经常会伴随至少一次的 Minor GC（但非绝对的，在Parallel Scavenge 收集器的收集策略里就有直接进行 Major GC 的策略选择过程）
  - 也就是在老年代空间不足时，会先尝试触发 Minor GC，如果之后空间还不足，则出发 Major GC
- Major GC 的速度一般会比 Minor GC 慢 10 倍以上，STW的时间更长
- 如果 Major GC后，内存还不足，就报 OOM 了



##### Full GC 触发机制

- 调用 System.gc() 时，系统建议执行 Full GC，但是不必然执行
- 老年代空间不足
- 方法区空间不足
- 通过 Minor GC后进入老年代的平均大小大于老年代的可用内存
- 由Eden区、survivor from 区向 surivor to 区复制时，对象大小大于 to 可用内存，则把该对象转存到老年代，且老年代的可用内存小于该对象大小

注意：full gc 是开发或调优中尽量要避免的



##### GC一般流程

在 new 一个对象的时候（大对象可能直接进入老年代）即将放入 Eden 区，发现 Eden 区装不下了，于是触发了 Minor GC，去将 Eden 、Surivor From 区的垃圾清除，存活的对象就往 Surivor To 区复制，Surivor From区中到达阀值（每次Minor GC 加1岁）的对象就往老年代上挪，这时如果老年代如果放不下，就会触发 Major GC 或 Full GC 去回收老年代的垃圾，就是一层一层的回收和腾出空间，最后实在无法腾出空间就抛 OOM 



















